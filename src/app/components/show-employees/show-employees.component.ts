import { Component, OnInit } from '@angular/core';
import {Employee} from "../../Employee";
//import { HttpClient } from '@angular/common/http';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable, of} from "rxjs";
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-show-employees',
  templateUrl: './show-employees.component.html',
  styleUrls: ['./show-employees.component.css']
})
export class ShowEmployeesComponent implements OnInit {
  employee : Employee;
  id : number;
  bearer : string;
  errorMessage : String;

  constructor(private http: HttpClient, private route:ActivatedRoute) {

    this.employee = new Employee(0, "", "","", "", "", "");
    this.errorMessage = "";
    this.bearer = 'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIzUFQ0dldiNno5MnlQWk1EWnBqT1U0RjFVN0lwNi1ELUlqQWVGczJPbGU0In0.eyJleHAiOjE2NDU2NDcwNzIsImlhdCI6MTY0NTYzMjY3MiwianRpIjoiZjhlMDlkYjUtNmY4ZS00ODZlLTliMmYtMThjODQ4ZjMzZWQ4IiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay5zenV0LmRldi9hdXRoL3JlYWxtcy9zenV0IiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjU1NDZjZDIxLTk4NTQtNDMyZi1hNDY3LTRkZTNlZWRmNTg4OSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVtcGxveWVlLW1hbmFnZW1lbnQtc2VydmljZSIsInNlc3Npb25fc3RhdGUiOiI5MTcwNWMxMS1iZGQ2LTQ3MmEtYjUyMy0zZWY1MDU5MjMzZDQiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwiZGVmYXVsdC1yb2xlcy1zenV0IiwidW1hX2F1dGhvcml6YXRpb24iLCJ1c2VyIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJ1c2VyIn0.NGE5E8cj8Q7q4O5eWp2uIi3qgMePawVJBRDZn-MvV3xFT5EZSYysZ-kAmYjDfEwYlzazLrIWD-uHqBA37MJ-A163_VPRsAoOwBAr40CsGJOTh03JFN_-JVwGaEdc5Byk9nEM-Ri5UEUW_D49uVR16cc2litvhmVKsSgcsARxTK40u-IVSiL8emHfT0EfA37NDkMd774Trs5FQl8iXth31E3acbBTNJCGtyWuP_r75YIKC1opqsD1y6IESKiaxFJR7yVexg36T0XmU1TLPO2OfkZlJttnKD5JFzIvdYXV2ivz2yX7uF7Tgg2D7yw72kLXgARFILLVM2B1f5KTGZjRFw';
    this.id = this.route.snapshot.params['id'];
    this.fetchEmployeeById();
  }

  ngOnInit(): void {
  }

  openDelete() {
      if(confirm("Möchten Sie den Mitarbeiter " + this.employee.firstName + " " + this.employee.lastName + " wirklich löschen?")) {
        this.http.delete('/backend/'+ this.id, {
          headers: new HttpHeaders()
            .set('Content-Type', 'application/json')
            .set('Authorization', `Bearer ${this.bearer}`)
        }).subscribe(
          {
            error: error => {
                this.errorMessage = "Datensatz konnte nicht gelöscht werden! " + error.message;
              // @ts-ignore
              document.getElementById("errorMsg").style.display = "block"
            }
          }
        );
// @ts-ignore
        document.getElementById("successMsg").style.display = "block";
      }

  }

  fetchEmployeeById() {
    var employeeById = this.http.get<Employee>('/backend/'+ this.id, {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer}`)
    });

    employeeById.subscribe((ployee: Employee) => {
      this.employee = ployee;
    },
      error => {
        this.errorMessage = "Datensatz konnte nicht geladen werden! " + error.message;
        // @ts-ignore
        document.getElementById("errorMsg").style.display = "block";
        var loeschBtn = document.getElementById("loeschen");
        // @ts-ignore
        loeschBtn.setAttribute('disabled', 'disabled');
        // @ts-ignore
        loeschBtn.classList.add("btn-secondary")
        // @ts-ignore
        var editBtn = document.getElementById("bearbeiten");
        // @ts-ignore
        editBtn.setAttribute('disabled', 'disabled');
        // @ts-ignore
        editBtn.classList.add("btn-secondary")

      });
  }

}
