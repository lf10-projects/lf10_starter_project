import {Component, Input, OnInit} from '@angular/core';
import {HttpClient, HttpEvent, HttpHeaders} from "@angular/common/http";
import {Employee} from "../../Employee";
import {catchError, empty, map, Observable, of} from "rxjs";
import { error } from '@angular/compiler/src/util';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.css']
})
export class StartComponent implements OnInit {
  isEdit:boolean=false;
  isSubmit:boolean=false;
  posts : any;
  delete : any;
  employees$: Observable<Employee[]>;
  employeeById$: Observable<Employee[]>;
  bearer: any;
  isNotFound="hidden";

  constructor(private http: HttpClient) {
    this.employees$ = of([]);
    this.employeeById$ = of([]);
    this.bearer = "eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIzUFQ0dldiNno5MnlQWk1EWnBqT1U0RjFVN0lwNi1ELUlqQWVGczJPbGU0In0.eyJleHAiOjE2NDU2NjQzNDYsImlhdCI6MTY0NTY0OTk0NiwianRpIjoiNmZjN2I4ZjQtNzMzNS00MzM4LTljZmYtYjk5MmQ1ZjZiN2MzIiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay5zenV0LmRldi9hdXRoL3JlYWxtcy9zenV0IiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjU1NDZjZDIxLTk4NTQtNDMyZi1hNDY3LTRkZTNlZWRmNTg4OSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVtcGxveWVlLW1hbmFnZW1lbnQtc2VydmljZSIsInNlc3Npb25fc3RhdGUiOiI2NGI1MTllMi0zZDA2LTQ1NDctYjcyNy00ODkyNGU4MTVjYzIiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwiZGVmYXVsdC1yb2xlcy1zenV0IiwidW1hX2F1dGhvcml6YXRpb24iLCJ1c2VyIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJ1c2VyIn0.DhLHE8UYakzvHA7l_NJorOxCIA9-M-JthtmmUyRuiH6tgn-HQJZyYDMFccH4h32p0hIETvbRgnhpwz7M54203SgLNsmyduNnSmwltw02g6rbFviz1VxnuBopa1Ho74MEBOsbtDbkLZ5Qq8PhLvJehfrSdUb1vl0Qx9eX-hctxCbTJvW-3mhaz_6dLNTXSujcY89_WkEyMYcZX8W0mT67ndpJ8EhMCwjSoFrig0lUb-RCLPSdzE8B8NF_yGpOofbbyTPiayb1RpbyFmPTglwUTBfA-aMd0EWx43FwZITaVuv1wms0E3pDbWYKwiDjAKErXQwmfZbXwXFUqpSwZetxkA";
  }

  ngOnInit(): void { this.fetchData();}

  searchEmployeeById():void {
    // @ts-ignore
    this.employees$ = this.findById(number);
  }

  showEmployee(): void {
    var number=(<HTMLInputElement>document.getElementById('searchElement')).value;
    console.log(number);
    if(number.length>0) {
      // @ts-ignore
      this.employeeById$ = this.findById(number);
    }
  }
  findById(id: number) {
    this.employeeById$ = this.http.get<Employee[]>('/backend', {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer}`)
    }).pipe(map(data => data.filter(employee => employee.id?.toFixed().includes(id.toString()))));
    var x = this.employeeById$;
    console.log(x);
    return this.employeeById$;
  }

  fetchData() {
    this.employees$ = this.http.get<Employee[]>('/backend', {
      headers: new HttpHeaders()
        .set('Content-Type', 'application/json')
        .set('Authorization', `Bearer ${this.bearer}`)
    });
  }
  ngAfterViewInit(): void {
  }
}
