import {Component} from "@angular/core";
import {FormBuilder} from '@angular/forms';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Employee} from "../../Employee";
import {Observable} from "rxjs";
import {Router} from "@angular/router";


@Component({
  selector: "app-add-new-employee",
  templateUrl: './add-new-employee.component.html',
  styleUrls: ['./add-new-employee.component.css']
})
export class AddNewEmployeeComponent {

  checkoutForm = this.formBuilder.group({
    lastName: "",
    firstName: "",
    street: "",
    postcode: "",
    city: "",
    phone: ""
  });

  constructor(
    private formBuilder: FormBuilder, private http: HttpClient, private router: Router
  ) {
  }

  // @ts-ignore
  addData(): Observable<Employee> {
    // Process checkout data here
    //console.warn('Your order has been submitted', this.checkoutForm.value);

    if (this.checkoutForm.value.firstName != "" && this.checkoutForm.value.lastName != "" &&
      this.checkoutForm.value.street != "" && this.checkoutForm.value.postcode != "" &&
      this.checkoutForm.value.city != "" && this.checkoutForm.value.phone != "") {
      //this.checkoutForm.reset();

      //console.log(JSON.stringify(this.checkoutForm.value));
      //console.log(this.checkoutForm.value);

      this.http.post<Employee>('/backend', JSON.stringify(this.checkoutForm.value), {
        headers: new HttpHeaders()
          .set('Content-Type', 'application/json')
          .set('Authorization', 'Bearer eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIzUFQ0dldiNno5MnlQWk1EWnBqT1U0RjFVN0lwNi1ELUlqQWVGczJPbGU0In0.eyJleHAiOjE2NDU2NTQ4MjcsImlhdCI6MTY0NTY0MDQyNywianRpIjoiNjkzMGM5MzAtMjkzNy00MjllLWEwMGMtMzNjMDcwNmY0OTY2IiwiaXNzIjoiaHR0cHM6Ly9rZXljbG9hay5zenV0LmRldi9hdXRoL3JlYWxtcy9zenV0IiwiYXVkIjoiYWNjb3VudCIsInN1YiI6IjU1NDZjZDIxLTk4NTQtNDMyZi1hNDY3LTRkZTNlZWRmNTg4OSIsInR5cCI6IkJlYXJlciIsImF6cCI6ImVtcGxveWVlLW1hbmFnZW1lbnQtc2VydmljZSIsInNlc3Npb25fc3RhdGUiOiI0MTFjNjYyOC1jYzcyLTRiN2ItODk0My00NjI2OWVmNmU4NmUiLCJhY3IiOiIxIiwiYWxsb3dlZC1vcmlnaW5zIjpbXSwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwiZGVmYXVsdC1yb2xlcy1zenV0IiwidW1hX2F1dGhvcml6YXRpb24iLCJ1c2VyIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiJ1c2VyIn0.eZqNbDiYZwCXekeonjuLs4fZCNmv-YgOS-ZRMXY22nPZKk81SFtnImgxmUXq2j1XbbMmyAtHtf7jJRYtOG8oWhh97e5AN_Qr2iIXTy8BADeE9FkNi9FbU5YPjxdRliQZX8AQrIDDEDsX8_A0MMX-fmvqfurMhBJFjz8UqtjOrOnvOcVO7LUAt-gz1i9U7vpgG7m9Ic9MFqBF8GaAbFvWS0Vv-MxFSQhu3V2unXRBXCqbSJLhiB92qjNxOpEWQlv-0_PUtQ-jcL0CMHl9MXL4gCfeL1SGH_TmEKqUpsoIThCPl4RJH--ubdXOaE67Wk0UFhTvF4xkLGN8D4mEFYuJfg')
      }).subscribe((value) => {console.log("post request" + value)});
      console.log()
      this.router.navigate(['/']);
    } else {
      this.changeInputBorderColor();
    }
  }

  private changeInputBorderColor() {
    if (this.checkoutForm.value.firstName == "") {
      // @ts-ignore
      document.getElementById('firstName').style.borderColor = "#ff0000";
    } else {
      // @ts-ignore
      document.getElementById('firstName').style.borderColor = "#4b4c4d";
    }
    if (this.checkoutForm.value.lastName == "") {
      // @ts-ignore
      document.getElementById('lastName').style.borderColor = "#ff0000";
    } else {
      // @ts-ignore
      document.getElementById('lastName').style.borderColor = "#4b4c4d";
    }
    if (this.checkoutForm.value.street == "") {
      // @ts-ignore
      document.getElementById('street').style.borderColor = "#ff0000";
    } else {
      // @ts-ignore
      document.getElementById('street').style.borderColor = "#4b4c4d";
    }
    if (this.checkoutForm.value.postcode == "") {
      // @ts-ignore
      document.getElementById('zip').style.borderColor = "#ff0000";
    } else {
      // @ts-ignore
      document.getElementById('zip').style.borderColor = "#4b4c4d";
    }
    if (this.checkoutForm.value.city == "") {
      // @ts-ignore
      document.getElementById('city').style.borderColor = "#ff0000";
    } else {
      // @ts-ignore
      document.getElementById('city').style.borderColor = "#4b4c4d";
    }
    if (this.checkoutForm.value.phone == "") {
      // @ts-ignore
      document.getElementById('phone').style.borderColor = "#ff0000";
    } else {
      // @ts-ignore
      document.getElementById('phone').style.borderColor = "#4b4c4d";
    }
  }
}
