import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';

import {AppComponent} from './app.component';
import {HttpClientModule} from "@angular/common/http";
import {HeaderComponent} from "./components/header/header.component";
import {ShowEmployeesComponent} from "./components/show-employees/show-employees.component";
import {StartComponent} from "./components/start/start.component";
import {AddNewEmployeeComponent} from "./components/add-new-employee/add-new-employee.component";
import {FooterComponent} from "./components/footer/footer.component";
import {FooterAbout} from "./components/footer/about/footer.about";
import {FooterPrivacy} from "./components/footer/privacy/footer.privacy";

import {Routes, RouterModule} from "@angular/router";
import {FormBuilder, FormsModule, ReactiveFormsModule} from "@angular/forms";

//Routing
const appRoutes: Routes = [
  {path:'', component:StartComponent},
  {path:'show-employee/:id', component:ShowEmployeesComponent},
  {path: 'about', component:FooterAbout},
  {path: 'privacy', component:FooterPrivacy},
  {path: 'add-new-employee', component:AddNewEmployeeComponent}
]

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    FooterPrivacy,
    FooterAbout,
    AppComponent,
    ShowEmployeesComponent,
    StartComponent,
    AddNewEmployeeComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    RouterModule.forRoot(appRoutes),
    FormsModule, ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
